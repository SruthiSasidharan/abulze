from django.urls import path
from .views import *

urlpatterns = [
    path("user-create", CreateUserProfileAPIView.as_view(), name="to_create_a_user_profile"),
    path("user-login", LoginView.as_view(), name="login_for_user"),
    path("address-create", AddressCreateAPIView.as_view(), name="to_create_the_address"),
    path("address-list/<int:id>", UserAddressListAPIView.as_view(), name="to_list_the_address"),
    path("address-update-delete/<int:pk>", AddressAPIView.as_view(), name="to_update_delete"),
    path("add-to-cart", AddToCartAPIView.as_view(), name="add_to_cart_view"),
    path("review/<int:id>", ReviewAPIview.as_view(), name="to_review"),
    path("cart-view/<int:id>", CartsAPIView.as_view(), name="view_cart"),
    path("orders", OrderAPIView.as_view(), name="place_an_order"),
    path("cart-delete/<int:id>", CartDeleteAPIView().as_view(), name="cart-delete"),
    path("all-product", ViewAllProducts.as_view(), name="view_all_products"),
    path("product/<int:pk>", ViewSingleProduct.as_view(), name="view_a_particular_product"),
    path("user-order/<int:id>", UserOrderAPIView().as_view(), name="user-order"),
    path("user-logout", LogoutView.as_view(), name="logout_function")

]
