from django.urls import path
from .views import *

urlpatterns = [
    path('add-category', CategoryAPIView.as_view(), name="add_categories"),
    path('add-product', ProductAPIView.as_view(), name="add_products"),
    path('update-product/<int:pk>', ProductUpdateAPIView.as_view(), name="edit_products"),
    path('update-category/<int:pk>', CategoryUpdateAPIView.as_view(), name="edit_categories"),
    path('order-view', AllOrderView.as_view(), name="view_all_orders"),
    path("order-status-update/<int:pk>", OrderStatusUpdateView.as_view(), name="update the order"),
    path("pending-order", PendingOrderView.as_view(), name="all_pending_orders"),
    path("completed-order", CompletedOrderView.as_view(), name="all_completed_orders"),
    path("admin-create", CreateAdminProfileAPIView.as_view(), name="to_create_a_user_profile"),
    path("admin-login", LoginAdminView.as_view(), name="login_for_user"),
    path("admin-logout", LogoutView.as_view(), name="logout_function"),
    path("order-single-view/<int:pk>", SingleOrderView.as_view(), name="view_all_orders")

]
