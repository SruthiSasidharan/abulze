from django.db import models


class Category(models.Model):
    category_name = models.CharField(max_length=50)
    description = models.CharField(max_length=200)


class Product(models.Model):
    category = models.ForeignKey(Category, on_delete=models.CASCADE)
    product_name = models.CharField(max_length=50)
    created_on = models.DateTimeField(auto_now=True)
    description = models.CharField(max_length=200)
    stock = models.IntegerField()
    price = models.FloatField()
    offer_price = models.FloatField(null=True)
    image = models.FileField(upload_to='gallaries', blank=True, null=True)  # to upload multiple image
