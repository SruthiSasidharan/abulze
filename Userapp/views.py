from django.db.models import Sum
from .serializers import *
from rest_framework.authtoken.models import Token
from django.contrib.auth import login, logout
from rest_framework.response import Response
from rest_framework.views import APIView
from django.contrib.auth.hashers import make_password, check_password
from rest_framework import generics, status


class CreateUserProfileAPIView(APIView):
    """
       View for admin Registration
    """
    permission_classes = []  # disables permission
    serializer_class = UserProfileCreateSerializer

    def post(self, request):
        data = request.data
        serializer = UserProfileCreateSerializer(data=data)
        if serializer.is_valid():
            password = request.data['password']
            confirm_password = request.data['confirm_password']
            if password == confirm_password:
                password1 = make_password(data['password'])  # encrypt the password
                password2 = make_password(data['confirm_password'])  # encrypt the password
                serializer.save(user=True, password=password1, confirm_password=password2)
                email = serializer.data['email']
                id = serializer.data['id']
                data = User.objects.get(email=email)
                result = {
                    "id": id,
                    "email": data.email,
                }

                return Response(result, status=200)
            else:
                return Response("wrong_password")
        else:
            return Response(serializer.errors, status=400)


class LoginView(APIView):
    """
        View for user login
    """
    permission_classes = []
    serializer_class = UserSerializer

    def post(self, request):
        email = request.data['email']
        password = request.data['password']
        try:
            user = User.objects.get(email=email)
            user_id = user.id
            user_email = user.email
            if user and check_password(password, user.password):
                login(request, user)
                result = {
                    'id': user_id,
                    'email': user_email,

                }
                return Response(result)
            return Response("Invalid password")
        except User.DoesNotExist:
            return Response('User Does not exists')


class AddressCreateAPIView(APIView):
    """
      View for address creation
    """
    serializer_class = AddressSerializers

    def post(self, request):
        serializer = AddressSerializers(data=request.data)
        if serializer.is_valid():
            serializer.save(user=request.user)
            return Response("address added")


class AddressAPIView(generics.RetrieveUpdateDestroyAPIView):
    """
    Address update delete view
    """
    serializer_class = AddressSerializers

    def get_queryset(self, ):
        queryset = Address.objects.all()
        return queryset

    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        self.perform_destroy(instance)
        return Response("Deleted successfully", status=status.HTTP_200_OK)


class UserAddressListAPIView(APIView):
    """
        View for user address
    """

    def get(self, request, id, *args, **kwargs):
        obj = Address.objects.filter(user=id)
        serializer = AddressSerializers(obj, many=True)
        data = serializer.data
        return Response(data)


class CartsAPIView(APIView):
    """
      Cart View
    """

    def get(self, request, id, *args, **kwargs):
        cart = Cart.objects.filter(user_id=id)
        serializer = CartAllSerializer(cart, many=True)
        return Response(serializer.data, status=200)


class AddToCartAPIView(APIView):
    """
      Add to cart view
    """
    serializer_class = CartSerializer

    def post(self, request):
        serializer = CartSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save(user_id=request.user)
            pro = serializer.data['product']
            sid = serializer.data['id']
            price = Cart.objects.get(id=sid).product.price
            quantity = serializer.data['quantity']
            data = Cart.objects.get(id=sid)
            total = price * quantity
            data.single_total = total
            data.save()
            price_total = Cart.objects.all().aggregate(total=Sum('single_total'))
            total_sum = price_total.get('total')
            data.sub_total = total_sum
            data.save()
            return Response("added to cart")
        return Response("not added")


class OrderAPIView(APIView):
    """
     place an order view
    """
    serializer_class = OrderSerializer

    def post(self, request):
        serializer = OrderSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save(user=request.user)
            sid = serializer.data['id']
            data = Order.objects.get(id=sid)
            total = Order.objects.get(id=sid).cart.sub_total
            data.final_order_amount = total
            data.save()
            return Response("order placed")
        return Response("not confirmed")


class CartDeleteAPIView(APIView):
    """
    Cart delete
    """

    def delete(self, request, id, format=None):
        try:
            obj = Cart.objects.get(id=id)
            obj.delete()
            return Response("Success")
        except:
            return Response("Failure")


class UserOrderAPIView(APIView):
    """
    User order history
    """

    def get(self, request, id, *args, **kwargs):
        history = Order.objects.filter(user_id=id)
        serializer = UserOrderSerializer(history, many=True)
        data = serializer.data
        return Response(data)


class ReviewAPIview(APIView):
    """
     Product Review view
    """
    serializer_class = ReviewSerializers

    def post(self, request, id):
        product = Product.objects.get(id=id)
        data = request.data
        serializer = ReviewSerializers(data=data)
        if serializer.is_valid():
            serializer.save()
            return Response("Review added")
        return Response("Review adding failed")


class ViewAllProducts(generics.ListAPIView):
    """
    view all products
    """
    queryset = Product.objects.all()
    serializer_class = ProductSerializer


class ViewSingleProduct(generics.RetrieveAPIView):
    """
    view particular product
    """
    queryset = Product.objects.all()
    serializer_class = ProductSerializer


class LogoutView(APIView):
    """
    Logout
    """

    def get(self, request):
        # request.user.auth_token.delete()
        logout(request)
        return Response('logged out')


